package core;

import java.io.*;

public class GitRepository {

	private  String repoPath;
	
	public GitRepository(String repoPath){
		
		this.repoPath=repoPath;
	}
	
	public String getHeadRef() throws Exception {
		
		BufferedReader bf= new BufferedReader(new FileReader(repoPath+"/HEAD"));
		
		return (bf.readLine()).substring(5);
	}
	public String getRefHash(String path) throws Exception {
		
		BufferedReader bf= new BufferedReader(new FileReader(repoPath+"/"+path));
		
		return bf.readLine();
	}
}
